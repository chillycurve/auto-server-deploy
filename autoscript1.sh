#!/bin/bash

sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get -y install openresolv curl linux-headers-$(uname -r) wireguard-dkms wireguard-tools acl software-properties-common net-tools mc
sudo apt-get purge do-agent -y
## if on digitalocean
curl -sSL https://repos.insights.digitalocean.com/install.sh | sudo bash
useradd serveradmin
usermod -aG sudo serveradmin
mkdir /home/serveradmin
rsync --archive --chown=serveradmin:serveradmin ~/.ssh /home/serveradmin
curl https://rclone.org/install.sh | sudo bash
sudo mkdir /root/.config/
sudo mkdir /root/.config/rclone/
sudo touch /root/.config/rclone/rclone.conf
sudo wget https://gitlab.com/chillycurve/auto-server-deploy/-/raw/master/rclone.conf -O /root/.config/rclone/rclone.conf
sudo mkdir /home/serveradmin/remote
rclone mount wasabi1:test1234543123 /home/serveradmin/remote --vfs-cache-mode full --allow-other &
sudo apt-get install docker.io -y
sudo systemctl enable --now docker
sudo usermod -aG docker serveradmin
sudo curl -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo wget https://gitlab.com/chillycurve/auto-server-deploy/-/raw/master/environment -O /etc/environment
export EXIP=$(curl ifconfig.io)
echo 'EXIP'=""$EXIP"" >> /etc/environment
echo 'LAN'="10.0.0.0/24" >> /etc/environment
chmod 755 /etc/environment
sudo chmod +x /usr/local/bin/docker-compose
sudo chmod -R 775 /home/serveradmin/remote/
wget https://gitlab.com/chillycurve/auto-server-deploy/-/raw/master/compose1.yml -O /home/serveradmin/compose1.yml
wget https://gitlab.com/chillycurve/auto-server-deploy/-/raw/master/wg0.conf -O /home/serveradmin/remote/docker/wireguard-client/wg0.conf
sudo -E docker-compose -f /home/serveradmin/compose1.yml up -d --remove-orphans
