#!/bin/bash

sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get -y install openresolv curl linux-headers-$(uname -r) wireguard-dkms wireguard-tools acl software-properties-common net-tools mc
sudo apt-get purge do-agent -y
## if on digitalocean
curl -sSL https://repos.insights.digitalocean.com/install.sh | sudo bash
wget https://gitlab.com/chillycurve/auto-server-deploy/-/raw/master/wg0.conf -O /etc/wireguard/wg0.conf
sudo chown root:root -R /etc/wireguard && sudo chmod 600 -R /etc/wireguard
sudo wg-quick up
useradd serveradmin
usermod -aG sudo serveradmin
mkdir /home/serveradmin
rsync --archive --chown=serveradmin:serveradmin ~/.ssh /home/serveradmin
curl https://rclone.org/install.sh | sudo bash
sudo mkdir /root/.config/
sudo mkdir /root/.config/rclone/
sudo touch /root/.config/rclone/rclone.conf
sudo wget https://gitlab.com/chillycurve/auto-server-deploy/-/raw/master/rclone.conf -O /root/.config/rclone/rclone.conf
sudo mkdir /home/serveradmin/remote
